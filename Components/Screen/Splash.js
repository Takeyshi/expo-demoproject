
import React, { Component } from 'react';
import { styles } from '../style/Style'
import {
  Dimensions,
  View,
  Image,
  AsyncStorage,
} from 'react-native';
import GLOBAL from '../glob/glob'
import { LinearGradient } from 'expo-linear-gradient';

import firebase from 'firebase';
import fire from '../glob/Fire';

const WIDTH = Dimensions.get('window').width
const HEIGHT = Dimensions.get('window').height
GLOBAL.WIDTH = WIDTH;
GLOBAL.HEIGHT = HEIGHT;
export default class FirstPage extends Component {

  constructor(props) {
    super(props)
    if (!firebase.apps.length) {
      firebase.initializeApp(fire.firebaseConfig);
    }
  }

  static navigationOptions = {
    header: null
  }
  componentWillMount() {
    setTimeout(() => {
      this.getUserId();
    }, 4000);
  }
  getUserId = async () => {
    let userId = '';
    try {
      userId = await AsyncStorage.getItem('userId') || 'none';
      console.log("TOKEN VALUE", GLOBAL.TOKEN)
      if (userId != 'none') {
        this.props.navigation.navigate('Signinsplash')
      }
      else {
        this.props.navigation.navigate('DrawerNavigator')
      }
    } catch (error) {
      console.log(error.message);
    }
    return userId;
  }

  render() {

    return (
      <View style={styles.Splashcontainer}>
        <LinearGradient colors={['white', 'lightgray', 'white']} style={styles.linearGradient} >
          <View style={styles.splashview}>
            <Image
              style={styles.splashlogo}
              source={require('../../assets/splash.png')}
            />

          </View>
        </LinearGradient>
      </View>
    );
  }
}


