import React, { Component } from 'react';
import {
  Text,
  View,
} from 'react-native';
import { StatusBar } from 'react-native';
import BackButton from '../Commons/BackButton';
import { styles } from '../style/Style'
import GLOBAL from '../glob/glob'

export default class SettingScreen extends Component {

  render() {
    GLOBAL.HEADERNAME = "Settings"
    return (
      <View
        style={styles.container}>
        <BackButton navigation={this.props.navigation} />
        <View style={styles.componentView} >
          <Text>Hello, Setting!</Text>
        </View>
      </View>
    );
  }
}
