import React, { Component } from 'react';
import {
  View,
  Image,
  KeyboardAvoidingView,
  Alert,
  Text,
  TextInput,
  Button,
  BackHandler,
  TouchableOpacity,
  AsyncStorage,
  Platform,
} from 'react-native';
import { LinearGradient } from 'expo-linear-gradient';
import { styles } from '../style/Style'
import GLOBAL from '../glob/glob'
import { ScrollView, } from 'react-native-gesture-handler';
export default class Login extends Component {
  static navigationOptions = {
    header: null
  }
  constructor(props) {
    super(props);
    this.state = {
      username: null,
      password: null,
      data: '',
      error: 'Please enter value in requiured fild'
    };
  }
  componentDidMount() {
    BackHandler.addEventListener('hardwareBackPress', this.handleBackButton);
  }
  componentWillUnmount() {
    BackHandler.removeEventListener('hardwareBackPress', this.handleBackButton);
  }

  handleBackButton() {
    //ToastAndroid.show('Exit Soon', ToastAndroid.SHORT);
    Alert.alert(
      'Exit App',
      'Are Your That You Want To Exit ?',
      [
        {
          text: 'Cancel',
          onPress: () => ToastAndroid.show('Stay With Us', ToastAndroid.SHORT),
          style: 'cancel',
        },
        { text: 'OK', onPress: () => BackHandler.exitApp() },
      ]
    );
    return true;
  }
  onLogin() {
    const { username, password, error } = this.state;
    GLOBAL.USERNAME = username;
    GLOBAL.PASSWORD = password;
    if (username != null) {
      if (password != null) {
        const { username, password } = this.state;
        const formData = new FormData
        formData.append('uname', username);
        formData.append('password', password);
        fetch('http://universalwebtech.com/mehul_hosting/skillmart/index.php/api/User/login', {
          method: 'POST',
          body: formData,
        }).then((response) => response.json())
          .then((responseJson) => {
            console.log(responseJson);
            if (responseJson.status === true) {
              GLOBAL.TOKEN = username + password;
              this.saveUserId();
              this.props.navigation.navigate('DrawerNavigator')
            } else {
              alert("Something Went Wrong ,Please try agin.")
            }
          })
          .catch((error) => {
            console.error(error);
          });

      }
      else {
        alert(error);
      }
    } else {
      alert(error);
    }
  }

  saveUserId = async () => {
    try {
      await AsyncStorage.setItem('userId', GLOBAL.TOKEN);
    } catch (error) {
      // Error retrieving data
      console.log(error.message);
    }
  };
  _Forgot() {
    this.props.navigation.navigate('Forgot')
  };
  _onPressButton = (val) => {
    if (val == 'Mobile') {
      this.props.navigation.navigate('Createnew')
    }
    else if (val == 'Google') {
      alert("Google")
    }
    else if (val == 'Facebook') {
      alert("Facebook")
    }
  }

  render() {
    GLOBAL.Login = this;
    return (
      <View style={styles.Splashcontainer}>
        <LinearGradient
          colors={['#F2C63F80', '#88867780', '#405D9780', '#6D496580', '#9C383680']}
          start={{ x: 1, y: 0 }}
          end={{ x: 0, y: 1 }}
          style={styles.linearGradient} >
          <View style={styles.Splashcontainer}>
            <KeyboardAvoidingView style={styles.keyboardavoid}
              behavior={Platform.OS == "ios" ? "padding" : "height"}>
              <View style={styles.loginimgview}>
                <Image
                  style={styles.loginimg}
                  source={require('../../assets/icon.png')}
                />
              </View>
              <TextInput
                style={styles.Textinput}
                placeholder="Username"
                placeholderTextColor={'#5E522C'}
                secureTextEntry={false}
                onChangeText={username => this.setState({ username })}
                value={this.state.username}
              />
              <TextInput
                style={styles.Textinput}
                placeholder="Password"
                placeholderTextColor={'#5E522C'}
                secureTextEntry={true}
                onChangeText={password => this.setState({ password })}
                value={this.state.password}
              />
              <View style={styles.forcre}>
                <TouchableOpacity
                  onPress={() => this._Forgot()}
                  style={styles.logtextfild} >
                  <Text style={styles.forgottext}>Forgot Password ? </Text>
                </TouchableOpacity>

              </View>

              <View style={styles.button}>
                <Button title="Login" onPress={this.onLogin.bind(this)} color="#2196F3" />
              </View>

              <View
                style={{ justifyContent: 'center', alignItems: 'center', marginBottom: 10, marginTop: 15 }}>
                <Text
                  style={{ textAlign: 'center', fontWeight: 'bold', fontSize: 16, color: '#5E522C' }}
                >====== Create New With ======</Text>
              </View>

              <View style={{
                height: 50,
                flexDirection: 'row',
                alignItems: 'center',
                marginTop: 35,
                justifyContent: 'space-around',
              }}>
                <TouchableOpacity
                  onPress={() => this._onPressButton('Google')}
                  style={{
                    width: 55,
                    height: 55,
                    borderWidth: 1,
                    borderColor: 'powderblue',
                    justifyContent: 'center',
                    alignItems: 'center',
                    borderRadius: 25
                  }} >
                  <Image
                    source={require('../image/google.png')}
                    style={{ height: 53, width: 53 }}
                  />
                </TouchableOpacity>
                <TouchableOpacity
                  onPress={() => this._onPressButton('Facebook')}
                  style={{
                    width: 55, height: 55, borderWidth: 1, borderColor: 'skyblue', borderRadius: 25, justifyContent: 'center',
                    alignItems: 'center',
                  }} >
                  <Image
                    source={require('../image/facebook.png')}
                    style={{ height: 53, width: 53 }}
                  />
                </TouchableOpacity>
                <TouchableOpacity
                  onPress={() => this._onPressButton('Mobile')}
                  style={{
                    width: 55, height: 55, borderWidth: 1, borderColor: 'steelblue', borderRadius: 25, justifyContent: 'center',
                    alignItems: 'center',
                  }} >
                  <Image
                    source={require('../image/mobile.png')}
                    style={{ height: 53, width: 53 }}
                  />
                </TouchableOpacity>

              </View>

            </KeyboardAvoidingView>


          </View>
        </LinearGradient>
      </View>
    );
  }
}
