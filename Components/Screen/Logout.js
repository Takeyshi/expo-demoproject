import React, { Component } from 'react';
import {
    Alert,
    BackHandler,
    ToastAndroid,
} from 'react-native';
import GLOBAL from '../glob/glob'

export default class Logout extends Component {
    componentWillUnmount() {
        this.alert();
    }
    ok() {
        GLOBAL.TOKEN == null,
            BackHandler.exitApp()
    }
    alert() {
        Alert.alert(
            'Exit App',
            'Are Your That You Want To Exit ?',
            [
                {
                    text: 'Cancel',
                    onPress: () => ToastAndroid.show('Stay With Us', ToastAndroid.SHORT),
                    style: 'cancel',
                },
                { text: 'OK', onPress: () => this.ok() },
            ]
        )
    }
    render() {
    }
}
