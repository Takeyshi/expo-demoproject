import React, { Component } from 'react';
import {
  Text,
  View,
  BackHandler,
  Alert,
  ToastAndroid
} from 'react-native';
import MenuButton from '../Commons/MenuButton';
import { styles } from '../style/Style'
import GLOBAL from '../glob/glob'


export default class HomeScreen extends Component {
  componentDidMount() {
    BackHandler.addEventListener('hardwareBackPress', this.handleBackButton);
  }
  componentWillUnmount() {
    BackHandler.removeEventListener('hardwareBackPress', this.handleBackButton);
  }

  handleBackButton() {
    //ToastAndroid.show('Exit Soon', ToastAndroid.SHORT);
    Alert.alert(
      'Exit App',
      'Are Your That You Want To Exit ?',
      [
        {
          text: 'Cancel',
          onPress: () => ToastAndroid.show('Stay With Us', ToastAndroid.SHORT),
          style: 'cancel',
        },
        { text: 'OK', onPress: () => BackHandler.exitApp() },
      ]
    );
    return true;
  }
  render() {

    return (
      <View
        style={styles.container}>
        <MenuButton navigation={this.props.navigation} />
        {console.log("USER",GLOBAL.USERNAME,"TOKEN",GLOBAL.TOKEN)}
        <View style={styles.componentView} >
          <Text>Hello, Home!</Text>
        </View>
      </View>
    );
  }
}
