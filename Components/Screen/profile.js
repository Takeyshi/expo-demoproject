import React, { Component } from 'react';
import {
  Text,
  View,
  TouchableOpacity,
  ScrollView,
  Image,
  FlatList,
  Alert
} from 'react-native';
import BackButton from '../Commons/BackButton';
import { profile, styles } from '../style/Style'
import GLOBAL from '../glob/glob'
import { LinearGradient } from 'expo-linear-gradient';

import * as ImagePicker from 'expo-image-picker';
import Constants from 'expo-constants';
import * as Permissions from 'expo-permissions';


export default class Profile extends Component {

  constructor() {
    super();
    this.state = {
      image: null,
    };
  }

  componentDidMount() {
    this.getPermissionAsync();
  }

  getPermissionAsync = async () => {
    if (Constants.platform.ios) {
      const { status } = await Permissions.askAsync(Permissions.CAMERA_ROLL);
      if (status !== 'granted') {
        alert('Sorry, we need camera roll permissions to make this work!');
      }
    }
  }

  _pickImage = async () => {
    let result = await ImagePicker.launchImageLibraryAsync({
      mediaTypes: ImagePicker.MediaTypeOptions.All,
      allowsEditing: true,
      aspect: [4, 3],
    });

    console.log(result);

    if (!result.cancelled) {
      this.setState({ image: result.uri });
      GLOBAL.IMAGE = this.state.image
    }
  }
  getListViewItem = (item) => {
    if (item.titledata == null) {
      alert(item.title);
    }
  }
  render() {
    GLOBAL.HEADERNAME = "Profile"
    let { image } = this.state;
    return (
      <View
        style={styles.container}>
        <BackButton navigation={this.props.navigation} />
        <View style={styles.componentView} >
          <View style={profile.profilephotoview}>
            <LinearGradient
              style={profile.profilelinearGradient}
              start={{ x: 1, y: 0 }}
              end={{ x: 0, y: 1 }}
              colors={[
                // 'lightgray',
                // '#FBBC05',
                // 'lightgray'
                '#F2C63F80', '#88867780', '#405D9780', '#6D496580', '#9C383680'
              ]} >
              <TouchableOpacity
                onPress={this._pickImage}
                style={profile.imagepickertouch}>
                <Image
                  source={{ uri: GLOBAL.IMAGE || image }}
                  style={profile.imagestyle}
                />
              </TouchableOpacity>
              <Text style={profile.belowprofile}>USERNAME</Text>
              <Text style={profile.belowprofile}>EMAIL ADDRESS</Text>
            </LinearGradient>
          </View>
          <View style={profile.infoview}>
            <View style={profile.mateareaview} >
              <Text style={profile.mateareanumber}>21</Text>
              <Text style={profile.matearea}>MATE CONTACT</Text>
            </View>
            <View style={profile.mateareaview} >
              <Text style={profile.mateareanumber}>07</Text>
              <Text style={profile.matearea}>AREA</Text>
            </View>
          </View>
          <View style={profile.userinfoview}>
            <ScrollView style={profile.userinfoscroll}>
              <FlatList
                data={[
                  { titledata: 'Dhurv Dave', title: 'User Name' }, { titledata: '30da121997', title: 'Password' }, { titledata: '+913974766500', title: 'Enter Contact Number' },
                  { titledata: 'Google', title: 'Login With' }, { titledata: '30-12-1997', title: 'Date Of Birth' }, { titledata: 'dvesp1901@gmail.com', title: 'Email Address' },
                  { titledata: { contact: '+91123456711' }, title: 'Mate Contact' }
                ]}
                renderItem={({ item }) =>
                  <TouchableOpacity
                    onPress={this.getListViewItem}
                    style={profile.userinfomain}>
                    <View style={profile.usertitlefild} >
                      <Text style={profile.userinfotitle}>{item.title}</Text>
                    </View>
                    <View style={profile.userinfofilddataview} >
                      <Text style={profile.userinfofildtext}>{item.title == 'Mate Contact' ? item.titledata.contact : item.titledata}</Text>
                    </View>
                  </TouchableOpacity>
                }
              />
            </ScrollView>
          </View>
        </View>
      </View>
    );
  }
}

