
import React, { Component } from 'react';
import { styles, signinsplash } from '../../style/Style'
import {
    Dimensions,
    View,
    Image,
    AsyncStorage,
    Text,
    TouchableOpacity,
} from 'react-native';
import GLOBAL from '../../glob/glob'
import { LinearGradient } from 'expo-linear-gradient';

const WIDTH = Dimensions.get('window').width
const HEIGHT = Dimensions.get('window').height
GLOBAL.WIDTH = WIDTH;
GLOBAL.HEIGHT = HEIGHT;
export default class Signinsplash extends Component {

    static navigationOptions = {
        header: null
    }

    _onPressButton = (val) => {
        if (val == 'Signup') {
            this.props.navigation.navigate('Createnew')
        }
        else if (val == 'Login') {
            this.props.navigation.navigate('Login')
        }
    }

    render() {

        return (
            <View style={styles.Splashcontainer}>
                <LinearGradient
                    colors={['#F2C63F80', '#88867780', '#405D9780', '#6D496580', '#9C383680']}
                    start={{ x: 1, y: 0 }}
                    end={{ x: 0, y: 1 }}
                    style={[styles.linearGradient, { alignItems: 'center' }]} >
                    <View style={{ flex: 1, justifyContent: 'center', alignItems: 'center', height: '100%', width: '100%' }}>
                        <View style={{ justifyContent: 'center', alignItems: 'center', height: '28%', width: '41%' }}>
                            <Image
                                style={{ width: '100%', height: '100%', resizeMode: 'stretch' }}
                                source={require('../../../assets/splash.png')}
                            />
                        </View>
                        <View style={{ flexDirection: 'row', justifyContent: 'center', marginTop: 15 }}>
                            <Text style={{ fontSize: 25, fontWeight: '600', textAlign: 'center', top: 5, }}>Welcome To</Text>
                            <Text style={{ fontSize: 28, fontWeight: '800', marginStart: 10, textAlign: 'center',color:'#5E522C' }}>TATVAMASI</Text>
                        </View>
                        <TouchableOpacity
                            onPress={() => this._onPressButton('Login')}
                            style={{ justifyContent: 'center', alignItems: 'center', width: '80%', height: 50, marginTop: 25, backgroundColor: '#405D9720', borderRadius: 25 }}>
                            <Text style={{ textAlign: 'center', fontWeight: '600', fontSize: 24, color: 'white' }}>Log in</Text>
                        </TouchableOpacity>

                        <TouchableOpacity
                            onPress={() => this._onPressButton('Signup')}
                            style={{ justifyContent: 'center', alignItems: 'center', width: '80%', height: 50, marginTop: 25, backgroundColor: '#405D9720', borderRadius: 25, }}>
                            <Text style={{ textAlign: 'center', fontWeight: '600', fontSize: 24, color: 'white' }}>Sign up</Text>
                        </TouchableOpacity>
                    </View>
                </LinearGradient>
            </View>
        );
    }
}
