
import React, { Component } from 'react';
import { styles, createnew } from '../../style/Style'
import {
  Dimensions,
  View,
  KeyboardAvoidingView,
  Alert,
  Text,
  TextInput,
  Button,
  BackHandler,
  AsyncStorage,
} from 'react-native';
import GLOBAL from '../../glob/glob'
import { LinearGradient } from 'expo-linear-gradient';
import BackButton from '../../Commons/BackButton'
import { CheckBox } from 'react-native-elements'
const WIDTH = Dimensions.get('window').width
const HEIGHT = Dimensions.get('window').height
GLOBAL.WIDTH = WIDTH;
GLOBAL.HEIGHT = HEIGHT;
export default class Createnew extends Component {

  static navigationOptions = {
    header: null
  }
  constructor(props) {
    super(props);
    this.state = {
      Email: null,
      DateOfBirth: null,
      password: null,
      Confirmpassword: null,
      error: 'Please enter this fild.'
    };
  }
  componentDidMount() {
    BackHandler.addEventListener('hardwareBackPress', this.handleBackButton);
  }
  componentWillUnmount() {
    BackHandler.removeEventListener('hardwareBackPress', this.handleBackButton);
  }

  handleBackButton() {
    //ToastAndroid.show('Exit Soon', ToastAndroid.SHORT);
    Alert.alert(
      'Exit App',
      'Are Your That You Want To Exit ?',
      [
        {
          text: 'Cancel',
          onPress: () => ToastAndroid.show('Stay With Us', ToastAndroid.SHORT),
          style: 'cancel',
        },
        { text: 'OK', onPress: () => BackHandler.exitApp() },
      ]
    );
    return true;
  }
  onLogin = () => {
    const { Email, DateOfBirth, password, Confirmpassword } = this.state
    if (Email, DateOfBirth, password, Confirmpassword != null) {
      GLOBAL.EMAIL = Email
      GLOBAL.DOB = DateOfBirth
      GLOBAL.PASS = password
      this.props.navigation.navigate('DrawerNavigator')
    }
    else {
      alert(this.state.error)
    }

  }
  render() {
    GLOBAL.HEADERNAME = "Create Account"
    return (
      <View style={styles.Splashcontainer}>
        <BackButton navigation={this.props.navigation} />
        <LinearGradient
          colors={['#F2C63F80', '#88867780', '#405D9780', '#6D496580', '#9C383680']}
          start={{ x: 1, y: 0 }}
          end={{ x: 0, y: 1 }}
          style={[styles.linearGradient]} >

          <View style={createnew.mainview}>

            <View style={createnew.textview}>
              <Text style={createnew.text}>Signup To Tatvamasi</Text>
            </View>

            <TextInput
              style={createnew.Textinput}
              placeholder="Enter Your Email"
              placeholderTextColor={'#5E522C'}
              onChangeText={Email => this.setState({ Email })}
              value={this.state.Email}
            />

            <TextInput
              style={createnew.Textinput}
              placeholder="Enter Your DOB"
              placeholderTextColor={'#5E522C'}
              secureTextEntry={false}
              onChangeText={DateOfBirth => this.setState({ DateOfBirth })}
              value={this.state.DateOfBirth}
            />

            <TextInput
              style={createnew.Textinput}
              placeholder="Enter Your Password"
              placeholderTextColor={'#5E522C'}
              secureTextEntry={true}
              onChangeText={Password => this.setState({ Password })}
              value={this.state.Password}
            />

            <TextInput
              style={createnew.Textinput}
              placeholder="Confirm Password"
              placeholderTextColor={'#5E522C'}
              secureTextEntry={true}
              onChangeText={Confirmpassword => this.setState({ Confirmpassword })}
              value={this.state.Confirmpassword}
            />

            <View style={createnew.button}>
              <Button title="Log in" onPress={this.onLogin.bind(this)} color="#2196F3" />
            </View>
          </View>
        </LinearGradient>
      </View>
    );
  }
}
