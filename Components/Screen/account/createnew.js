
import React, { Component } from 'react';
import { styles, createnew } from '../../style/Style'
import {
  Dimensions,
  View,
  KeyboardAvoidingView,
  Alert,
  Text,
  TextInput,
  Button,
  BackHandler,
  AsyncStorage,
} from 'react-native';
import GLOBAL from '../../glob/glob'
import { LinearGradient } from 'expo-linear-gradient';
import { CheckBox } from 'react-native-elements'
import firebase from 'firebase';
import BackButton from '../../Commons/BackButton'
const WIDTH = Dimensions.get('window').width
const HEIGHT = Dimensions.get('window').height
GLOBAL.WIDTH = WIDTH;
GLOBAL.HEIGHT = HEIGHT;
export default class Createnew extends Component {

  static navigationOptions = {
    header: null
  }
  constructor(props) {
    super(props);
    this.state = {
      Fullname: null,
      Username: null,
      Contact: null,
      OTP: null,
      Hideview: false,
      error: 'Please enter this fild.'
    };
  }
  componentDidMount() {
    BackHandler.addEventListener('hardwareBackPress', this.handleBackButton);
  }
  componentWillUnmount() {
    BackHandler.removeEventListener('hardwareBackPress', this.handleBackButton);
  }

  handleBackButton() {
    //ToastAndroid.show('Exit Soon', ToastAndroid.SHORT);
    Alert.alert(
      'Exit App',
      'Are Your That You Want To Exit ?',
      [
        {
          text: 'Cancel',
          onPress: () => ToastAndroid.show('Stay With Us', ToastAndroid.SHORT),
          style: 'cancel',
        },
        { text: 'OK', onPress: () => BackHandler.exitApp() },
      ]
    );
    return true;
  }
  onSendOTP = () => {
    const { Fullname, Username, Contact } = this.state
    if (Fullname, Username, Contact != null) {

      firebase
        .database()
        .ref('users/' + Username)
        .set({
          Fullname: Fullname,
          Username: Username,
          Contact: Contact,
        });

      alert('OTP SEND')
      this.setState({ Hideview: true })
    } else {
      alert(this.state.error)
    }
  }

  onnext = async () => {
    if (this.state.OTP != null) {
      const { Fullname, Username, Contact, OTP } = this.state
      GLOBAL.FULLNAME = Fullname
      GLOBAL.USERNAME = Username
      GLOBAL.CONTACT = Contact
      this.props.navigation.navigate('Createnew1')
    } else {
      alert(this.state.error)
    }

  }
  render() {
    GLOBAL.HEADERNAME = "Create Account"
    return (
      <View style={styles.Splashcontainer}>
        <BackButton navigation={this.props.navigation} />
        <LinearGradient
          colors={['#F2C63F80', '#88867780', '#405D9780', '#6D496580', '#9C383680']}
          start={{ x: 1, y: 0 }}
          end={{ x: 0, y: 1 }}
          style={[styles.linearGradient]} >
          <View style={createnew.mainview}>

            <View style={createnew.textview}>
              <Text style={createnew.text}>Signup To Tatvamasi</Text>
            </View>


            <TextInput
              style={createnew.Textinput}
              placeholder="Enter Your Fullname"
              placeholderTextColor={'#5E522C'}
              secureTextEntry={false}
              onChangeText={Fullname => this.setState({ Fullname })}
              value={this.state.Fullname}
            />

            <TextInput
              style={createnew.Textinput}
              placeholder="Enter Your Username"
              placeholderTextColor={'#5E522C'}
              secureTextEntry={false}
              onChangeText={Username => this.setState({ Username })}
              value={this.state.Username}
            />

            <TextInput
              style={createnew.Textinput}
              placeholder="Enter Your Contact Number"
              placeholderTextColor={'#5E522C'}
              secureTextEntry={false}
              onChangeText={Contact => this.setState({ Contact })}
              value={this.state.Contact}
            />
            <View style={createnew.button}>
              <Button title="Send OTP" onPress={this.onSendOTP.bind(this)} color="#2196F3" />
            </View>
            {this.state.Hideview == true ? (
              <View style={{ width: '100%', justifyContent: 'center', alignItems: 'center' }}>
                <TextInput
                  style={createnew.Textinput}
                  placeholder="Enter Your OTP"
                  placeholderTextColor={'#5E522C'}
                  secureTextEntry={true}
                  onChangeText={OTP => this.setState({ OTP })}
                  value={this.state.OTP}
                />
                <View style={createnew.button}>
                  <Button title="NEXT" onPress={this.onnext.bind(this)} color="#2196F3" />
                </View>
              </View>
            )
              : null
            }
          </View>
        </LinearGradient>
      </View>
    );
  }
}
