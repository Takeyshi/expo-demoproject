
import React, { Component } from 'react';
import { styles, createnew } from '../../style/Style'
import {
  Dimensions,
  View,
  Alert,
  Text,
  TextInput,
  Button,
  BackHandler,
  AsyncStorage,
} from 'react-native';
import GLOBAL from '../../glob/glob'
import { LinearGradient } from 'expo-linear-gradient';
import BackButton from '../../Commons/BackButton'
const WIDTH = Dimensions.get('window').width
const HEIGHT = Dimensions.get('window').height
GLOBAL.WIDTH = WIDTH;
GLOBAL.HEIGHT = HEIGHT;
export default class Forgot extends Component {

  static navigationOptions = {
    header: null
  }
  constructor(props) {
    super(props);
    this.state = {
      Username: null,
      Contact: null,
      Email:null,
      error: 'Oops, Something went Wrong please check all Fields.'
    };
  }
  componentDidMount() {
    BackHandler.addEventListener('hardwareBackPress', this.handleBackButton);
  }
  componentWillUnmount() {
    BackHandler.removeEventListener('hardwareBackPress', this.handleBackButton);
  }

  handleBackButton() {
    Alert.alert(
      'Exit App',
      'Are Your That You Want To Exit ?',
      [
        {
          text: 'Cancel',
          onPress: () => ToastAndroid.show('Stay With Us', ToastAndroid.SHORT),
          style: 'cancel',
        },
        { text: 'OK', onPress: () => BackHandler.exitApp() },
      ]
    );
    return true;
  }
  onSendMail = () => {
    const { Email, Username, Contact } = this.state
    if (Username, Contact, Email != null) {
      this.props.navigation.navigate('Login')
    } else {
      alert(this.state.error)
    }
  }


  render() {
    GLOBAL.HEADERNAME = "Forgot "
    return (
      <View style={styles.Splashcontainer}>
        <BackButton navigation={this.props.navigation} />
        <LinearGradient
          colors={['#F2C63F80', '#88867780', '#405D9780', '#6D496580', '#9C383680']}
          start={{ x: 1, y: 0 }}
          end={{ x: 0, y: 1 }}
          style={[styles.linearGradient]} >
          <View style={createnew.mainview}>

            <View style={createnew.textview}>
              <Text style={createnew.fogotpage}>Enter below ask detail and make sure that those detail are link with your account.We'll send your login detail via mail if all your information are link with your account.</Text>
            </View>


            <TextInput
              style={createnew.Textinput}
              placeholder="Enter Your Username"
              placeholderTextColor={'#5E522C'}
              secureTextEntry={false}
              onChangeText={Username => this.setState({ Username })}
              value={this.state.Username}
            />

            <TextInput
              style={createnew.Textinput}
              placeholder="Enter Your Contact Number"
              placeholderTextColor={'#5E522C'}
              secureTextEntry={false}
              onChangeText={Contact => this.setState({ Contact })}
              value={this.state.Contact}
            />

            <TextInput
              style={createnew.Textinput}
              placeholder="Enter Your Email Address"
              placeholderTextColor={'#5E522C'}
              secureTextEntry={false}
              onChangeText={Email => this.setState({ Email })}
              value={this.state.Email}
            />

            <View style={createnew.button}>
              <Button title="Send Mail" onPress={this.onSendMail.bind(this)} color="#2196F3" />
            </View>

          </View>


        </LinearGradient>
      </View>
    );
  }
}
