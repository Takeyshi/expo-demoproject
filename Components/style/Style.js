import {
    StyleSheet,
    StatusBar
} from 'react-native';
import GLOBAL from '../glob/glob'

const styles = StyleSheet.create({
    container: {
        flex: 1,
        flexDirection: 'column',
        justifyContent: 'space-between',
    },
    fullmenu: {
        flexDirection: 'row',
        marginTop: StatusBar.currentHeight
    },
    menu: {
        width: '20%',
        height: 50,
        backgroundColor: '#006e61',
        justifyContent: 'center',
        alignItems: 'center',
    },
    menuIcon: {
        zIndex: 9,
        justifyContent: 'center',
        alignContent: 'center',
    },
    mainView: {
        width: '80%',
        height: 50,
        backgroundColor: '#006e61',
        justifyContent: 'center',

    },
    componentView: {
        flex: 0.98,
        backgroundColor: 'powderblue',
        marginBottom: 5,
        justifyContent: 'center',
        alignItems: 'center'
    },
    Splashcontainer:
    {
        flex: 1,
        justifyContent: 'center',
        alignItems: 'center',
    },
    linearGradient:
    {
        flex: 1,
        width: '100%',
        height: '100%',
    },
    splashlogo:
    {
        width: "60%",
        height: '35%',
        resizeMode: 'stretch',
    },
    splashview:
    {
        flex: 1,
        justifyContent: 'center',
        alignItems: 'center',
    },
    Textinput: {
        height: 50,
        borderBottomWidth: 2,
        borderBottomColor: '#5E522C',
        fontSize: 16,
        padding: 5,
        marginBottom: 10
    },
    button: {
        width: '100%',
        justifyContent: 'center',
        alignItems: 'center'
    },
    loginimgview: {
        justifyContent: 'center',
        alignItems: 'center',
        height:'35  %',
        width:'80%',
        marginStart:25

    },
    loginimg: {
        width: "100%",
        height: '130%',
        resizeMode: 'stretch',
    },
    keyboardavoid: {
        height: '80%',
        width: '80%',
        justifyContent: 'center',
    },
    forcre: {
        flexDirection: 'row',
        marginBottom: 10
    },
    logtextfild: {
        flex: 1,
        height: 25,
        textAlign: 'center',
        justifyContent: 'center'
    },
    forgottext: {
        color: 'red',
        fontWeight: 'bold',
        textAlign: 'right',
        fontSize: 16
    },
    createtext: {
        color: 'white',
        fontWeight: 'bold',
        textAlign: 'center',
        fontSize: 16
    },
    headertext: {
        fontSize: 22,
        fontWeight: 'bold',
        color: 'white',
        textAlign: 'left',
        marginStart: 50,
    },
    Loginsocial: {
        height: 50,
        flexDirection: 'row',
        alignItems: 'center',
        marginTop: 15,
        justifyContent: 'space-around',
    },
    Loginsocialbutton: {
        width: 55,
        height: 55,
        borderWidth: 1,
        borderRadius: 25
    },
})
const menuDrawer = StyleSheet.create({
    sideMenuContainer: {
        width: '100%',
        height: '100%',

        alignItems: 'center',
        paddingTop: 20,
    },
    sideMenuProfileIcon: {
        width: '100%',
        height: 140,
        justifyContent: 'flex-end',
        alignItems: 'flex-start',
        backgroundColor: '#006e61',
    },
    footer: {
        height: 50,
        flexDirection: 'row',
        alignItems: 'center',
        backgroundColor: 'powderblue',
        borderTopColor: 'lightgray',
        borderTopWidth: 1,
    },
    description: {
        flex: 1,
        marginLeft: 20,
        fontSize: 16
    },
    version: {
        flex: 1,
        textAlign: 'right',
        marginRight: 20,
        color: 'gray',
    },
    midview: {
        width: '100%',
        flex:1,
        backgroundColor: 'skyblue'
    },
    imagepickerview: {
        height: 75,
        width: 75,
        borderColor: 'white',
        borderRadius: 75 / 2,
        borderWidth: 2,
        marginStart: 30,
        marginBottom: 5,
        justifyContent: 'center',
        alignItems: 'center'
    },
    imagepickerimage: {
        width: 72,
        height: 72,
        borderRadius: 72
    },
    imagepickerusermail: {
        marginBottom: 5,
        marginStart: 30
    },
    socialmainview: {
        flex: 1,
        flexDirection: 'row',
        alignItems:'flex-end',
        marginBottom:25,
        justifyContent: 'space-around',
        marginTop: 15
    },
    socialsubview: {
        width: 50,
        height: 50,
        justifyContent: 'center',
        alignItems: 'center'
    },
    socialimage: {
        height: 45,
        width: 45
    },
    logouttext: {
        fontSize: 17,
        color: 'white',
    },
    logouttouch: {
        flexDirection: 'row',
        marginTop: 10
    },
    logoutview: {
        marginRight: 17,
        marginLeft: 22,
    },
    logoutimage: {
        height: 28,
        width: 28
    },
    Shareimage: {
        height: 22,
        width: 22
    },

})
const profile = StyleSheet.create({

    profilephotoview: {
        width: '100%',
        height: '30%',
    },
    profilelinearGradient:
    {
        flex: 1,
        width: '100%',
        height: '100%',
        justifyContent: 'center',
        alignItems: 'center'
    },
    imagepickertouch: {
        height: 100,
        width: 100,
        borderColor: 'white',
        borderRadius: 100 / 2,
        borderWidth: 2,
        marginStart: 30,
        marginBottom: 10,
        justifyContent: 'center',
        alignItems: 'center'
    },
    imagestyle: {
        width: 97,
        height: 97,
        borderRadius: 97
    },
    belowprofile: {
        marginBottom: 5,
        marginStart: 30
    },
    infoview: {
        flexDirection: 'row',
        justifyContent: 'center'
    },
    matearea: {
        textAlign: 'center',
        color: 'white',
        fontWeight: '400'
    },
    mateareanumber: {
        textAlign: 'center',
        color: 'white',
        fontWeight: '800',
        fontSize: 22
    },
    mateareaview: {
        width: '50%',
        height: 55,
        backgroundColor: 'green',
        borderBottomWidth: 1,
        borderBottomColor: 'white',
        borderTopWidth: 1,
        borderTopColor: 'white'
    },
    userinfoview: {
        flex: 1,
        flexDirection: 'row',
        justifyContent: 'center',
        height: '61%',
        backgroundColor: 'lightgray',
        width: '100%'
    },
    userinfoscroll: {
        marginTop: 10,
        marginBottom: 20
    },
    userinfomain: {
        flexDirection: 'column',
        alignItems: 'center',
        top: 15
    },
    usertitlefild: {
        width: '90%',
        height: 18,
    },
    userinfotitle: {
        textAlign: 'left',
        fontSize: 16,
        fontWeight: '400',
        color: 'steelblue'
    },
    userinfofilddataview: {
        width: '90%',
        height: 45,
        borderBottomColor: 'gray',
        borderBottomWidth: 1,
        justifyContent: 'center'
    },
    userinfofildtext: {
        textAlign: 'left',
        fontSize: 19,
        fontWeight: '700'
    },

})
const createnew = StyleSheet.create({
    mainview:{ justifyContent: 'center', alignItems: 'center', flex: 1, width: '100%' },
    Textinput: {
        height: 50,
        width: '80%',
        borderBottomWidth: 2,
        marginTop: 10,
        borderBottomColor: '#5E522C',
        fontSize: 16,
        padding: 5,
        marginBottom: 5
    },
    button: {
        width: '100%',
        justifyContent: 'center',
        alignItems: 'center',
        marginTop: 20
    },
    textview: {
        justifyContent: 'center',
        alignItems: 'center',
        marginBottom:25,
    },
    text:{
        fontSize:25,
        fontWeight:'600',
        textAlign:'center'
    },
    fogotpage:{
        fontSize:16,
        fontWeight:'200',
        textAlign:'center'
    }
})
export { styles, menuDrawer, profile, createnew }           