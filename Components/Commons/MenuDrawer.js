//This is an example code for Navigation Drawer with Custom Side bar//
import React, { Component } from 'react';
import {
    View,
    StyleSheet,
    Image, Text,
    TouchableOpacity,
    Alert,
    ToastAndroid,
    BackHandler,
    AsyncStorage,
} from 'react-native';
import { Icon } from 'react-native-elements';
import GLOBAL from '../glob/glob'
import { menuDrawer } from '../style/Style'

import * as ImagePicker from 'expo-image-picker';
import Constants from 'expo-constants';
import * as Permissions from 'expo-permissions';


export default class CustomSidebarMenu extends Component {
    constructor() {
        super();
        this.state = {
            image: null,

        };
        this.items = [
            {
                navOptionThumb: 'home',
                navOptionName: 'Home',
                screenToNavigate: 'Home',
            },
            {
                navOptionThumb: 'person',
                navOptionName: 'Profile',
                screenToNavigate: 'Profile',
                type: 'ionicons'
            },
            {
                navOptionThumb: 'mail',
                navOptionName: 'SMS-Alert',
                screenToNavigate: 'Link',
                type: 'Entypo'
            },
            {
                navOptionThumb: 'mic',
                navOptionName: 'Voice Activation',
                screenToNavigate: 'Setting',
                type: 'Ionicons'
            },
            {
                navOptionThumb: 'navigation',
                navOptionName: 'Area',
                screenToNavigate: 'Link',
                type: 'MaterialIcons'
            },
            {
                navOptionThumb: 'alarm',
                navOptionName: 'Timer',
                screenToNavigate: 'Setting',
                type: 'Ionicons'
            }, {
                navOptionThumb: 'people',
                navOptionName: 'Mate',
                screenToNavigate: 'Link',
                type: 'Ionicons'
            },

        ];

    }

    componentDidMount() {
        this.getPermissionAsync();
    }

    getPermissionAsync = async () => {
        if (Constants.platform.ios) {
            const { status } = await Permissions.askAsync(Permissions.CAMERA_ROLL);
            if (status !== 'granted') {
                alert('Sorry, we need camera roll permissions to make this work!');
            }
        }
    }

    _pickImage = async () => {
        let result = await ImagePicker.launchImageLibraryAsync({
            mediaTypes: ImagePicker.MediaTypeOptions.All,
            allowsEditing: true,
            aspect: [4, 3],
        });

        console.log(result);

        if (!result.cancelled) {
            this.setState({ image: result.uri });
            GLOBAL.IMAGE = this.state.image
        }
    }

    deleteUserId = async () => {
        try {
            GLOBAL.TOKEN == null
            await AsyncStorage.removeItem('userId');
            BackHandler.exitApp()
        } catch (error) {
            console.log(error.message);
            alert("Oops, Something Went Wrong!")
        }

    }
    alert() {
        Alert.alert(
            'Exit App',
            'Are Your That You Want To Exit ?',
            [
                {
                    text: 'Cancel',
                    onPress: () => ToastAndroid.show('Stay With Us', ToastAndroid.SHORT),
                    style: 'cancel',
                },
                { text: 'OK', onPress: () => this.deleteUserId() },
            ]
        )
    }

    Share() {
        alert("Share Application")
    }

    render() {
        let { image } = this.state;
        return (
            <View style={menuDrawer.sideMenuContainer}>

                {/* <Image
                    source={require('../image/user.png')}
                    style={menuDrawer.sideMenuProfileIcon}
                /> */}
                <View style={menuDrawer.sideMenuProfileIcon}>
                    <TouchableOpacity
                        onPress={this._pickImage}
                        style={menuDrawer.imagepickerview}>
                        <Image
                            source={{ uri: GLOBAL.IMAGE || image }}
                            style={menuDrawer.imagepickerimage}
                        />
                    </TouchableOpacity>
                    <Text style={menuDrawer.imagepickerusermail}>USERNAME</Text>
                    <Text style={menuDrawer.imagepickerusermail}>EMAIL ADDRESS</Text>
                </View>

                <View
                    style={{
                        width: '100%',
                        height: 3,
                        backgroundColor: 'black',
                    }}
                />

                <View style={menuDrawer.midview}>
                    {this.items.map((item, key) => (
                        <TouchableOpacity
                            style={{
                                flexDirection: 'row',
                                alignItems: 'center',
                                paddingTop: 10,
                                paddingBottom: 7,
                                backgroundColor: global.currentScreenIndex === key ? '#e0dbdb' : 'skyblue',
                            }}
                            key={key}>
                            <View style={{ marginRight: 17, marginLeft: 22 }}>
                                <Icon name={item.navOptionThumb} size={25} color="white" type={item.type} />
                            </View>
                            <Text
                                style={{
                                    fontSize: 17,
                                    color: global.currentScreenIndex === key ? 'red' : 'white',
                                }}
                                onPress={() => {
                                    global.currentScreenIndex = key;
                                    this.props.navigation.navigate(item.screenToNavigate);
                                }}>
                                {item.navOptionName}
                            </Text>
                        </TouchableOpacity>

                    ))}
                    <TouchableOpacity
                        style={menuDrawer.logouttouch}
                        onPress={() => this.Share()}>
                        <View style={menuDrawer.logoutview}>
                            <Image
                                source={require('../image/share.png')}
                                style={menuDrawer.Shareimage}
                                tintColor='white'
                            />
                        </View>
                        <Text style={menuDrawer.logouttext}>Share App</Text>
                    </TouchableOpacity>

                    <TouchableOpacity
                        style={menuDrawer.logouttouch}
                        onPress={() => this.alert('Logout')}>
                        <View style={menuDrawer.logoutview}>
                            <Image
                                source={require('../image/logout.png')}
                                style={menuDrawer.logoutimage}
                                tintColor='white'
                            />
                        </View>
                        <Text style={menuDrawer.logouttext}>Log-out</Text>
                    </TouchableOpacity>
                    <View style={menuDrawer.socialmainview}>
                        <View style={menuDrawer.socialsubview} >
                            <Image
                                source={require('../image/facebook.png')}
                                style={menuDrawer.socialimage}
                            />
                        </View>
                        <View style={menuDrawer.socialsubview}  >
                            <Image
                                source={require('../image/instagram.png')}
                                style={menuDrawer.socialimage}
                            />
                        </View>
                        <View style={menuDrawer.socialsubview}  >
                            <Image
                                source={require('../image/twitter.png')}
                                style={menuDrawer.socialimage}
                            />
                        </View>
                    </View>
                </View>


                <View style={menuDrawer.footer}>
                    <Text style={menuDrawer.description}>Tatvamasi</Text>
                    <Text style={menuDrawer.version}>V0.0.1</Text>

                </View>
            </View>
        );
    }
}
