import React from 'react';
import {
    TouchableOpacity,
    View,
    Text,
} from 'react-native';
import { styles } from '../style/Style'
import { Icon } from 'react-native-elements'
import GLOBAL from '../glob/glob'

export default class MenuButton extends React.Component {
    render() {
        return (
            <View style={styles.fullmenu}>
                <TouchableOpacity
                    onPress={() => this.props.navigation.toggleDrawer()}
                    style={styles.menu}>
                    <Icon
                        name="list"
                        color='white'
                        size={40}
                        style={styles.menuIcon}
                    />
                </TouchableOpacity>
                <TouchableOpacity style={styles.mainView} >
                    <Text style={styles.headertext}>{GLOBAL.HEADERNAME}</Text>
                </TouchableOpacity>
            </View>
        )
    }
}


