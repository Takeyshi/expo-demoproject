import { createAppContainer } from 'react-navigation';
import { createStackNavigator } from 'react-navigation-stack';


import Splash from '../Screen/Splash';
import Signinsplash from '../Screen/account/signinsplash';
import Createnew from '../Screen/account/createnew';
import Createnew1 from '../Screen/account/createnew1';
import Forgot from '../Screen/account/forgot';
import BackButton from '../Commons/BackButton';

import DrawerNavigator from './DrawerNavigator';
import Login from '../Screen/Login';


const AppNavigator = createStackNavigator(
     {
          Splash: Splash,
          Signinsplash: Signinsplash,
          Createnew: Createnew,
          Createnew1: Createnew1,
          Forgot: Forgot,
          Login: Login,
          BackButton: BackButton,
          DrawerNavigator: DrawerNavigator,

     },
     {
          headerMode: 'none',
          initialRouteName: 'Splash'
     },
);
export default createAppContainer(AppNavigator);  
