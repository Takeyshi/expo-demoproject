import React from 'react';
import { platform, Dimensions } from 'react-native';

import { createAppContainer } from 'react-navigation';
import { createDrawerNavigator } from 'react-navigation-drawer';
import HomeScreen from '../Screen/HomeScreen';
import LinkScreen from '../Screen/LinkScreen';
import SettingScreen from '../Screen/SettingScreen';
import ProfileScreen from '../Screen/profile';
import Logout from '../Screen/Logout'
import MenuDrawer from '../Commons/MenuDrawer';


const WIDTH = Dimensions.get('window').width;
const DrawerConfig = {
    drawerwidth: WIDTH * 0.83,
    contentComponent: ({ navigation }) => {
        return (<MenuDrawer navigation={navigation} />)
    }
}

const DrawerNavigator = createDrawerNavigator(
    {
        Home: {
            screen: HomeScreen
        },
        Link: {
            screen: LinkScreen
        },
        Setting: {
            screen: SettingScreen
        },
        Profile: {
            screen: ProfileScreen
        },
        Logout: {
            screen: Logout
        },
        headerMode: 'none',
        initialRouteName: 'Home'


    },
    DrawerConfig
)

export default createAppContainer(DrawerNavigator);