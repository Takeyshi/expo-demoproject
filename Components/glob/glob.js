module.exports = {
  USERNAME: null,
  PASSWORD: null,
  EMAIL: null,
  DOB: null,
  FULLNAME: null,
  CONTACT: null,

  TOKEN: null,
  HEIGHT: null,
  WIDTH: null,
  IMAGE: null,
  HEADERNAME: "Home",
}