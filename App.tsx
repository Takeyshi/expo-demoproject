import React from 'react';
import { StyleSheet, Text, View } from 'react-native';


import App0 from './Components/navigation/App';


export default function App() {
  return (
    <View style={styles.container}>
      <App0/>
    </View>
  );
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: '#fff',  
   
  },
});
